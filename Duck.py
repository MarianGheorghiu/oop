class Duck:
    def quack(self):
        print('Quack, quack!')


class Person:
    def quack(self):
        print("I'm a Quackin' ! ")


def in_the_forest(mallard):
    mallard.quack()
