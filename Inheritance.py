class Animal():

    def __init__(self):
        print('Animal created.')

    def who_am_i(self):
        print('I am an amimal.')

    def eat(self):
        print('I am eating.')


my_animal = Animal()
my_animal.eat()


class Dog(Animal):

    def __init__(self):

        Animal.__init__(self)

        print('Dog created.')

    def bark(self):
        print('Woof!')


my_dog = Dog()
my_dog.eat()
my_dog.who_am_i()
my_dog.bark()
