class Time(object):
    def __init__(self, hour=0, minute=0):
        self.hour = hour
        self.minute = minute

    @classmethod
    def from_string(time_str):
        hour, minute = map(int, time_str.split(':'))
        return (hour, minute)

    @staticmethod
    def is_valid(time_str):
        hour, minute = map(int, time_str.split(':'))
        return hour <= 23 and minute <= 59

    def display():
        print(from_string('10:15'))
        print(is_valid('10:15'))
